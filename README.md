A continuación encontrará la información para poder realizar la instalación del tema para WordPress "Giving Theme"

# Giving Theme
  
**Wordpress Version:** WordPress 5.4.2  
**Requires PHP:** 5.6   
**License:** GNU General Public License v2 or later   
**License URI:** http://www.gnu.org/licenses/gpl-2.0.html  

![Giving Theme WordPress](https://bitbucket.org/jdda95/giving-theme/raw/7df3c398f38761fe048ca20f3ba5ad0a41e274ef/wp-content/themes/Giving-theme/screenshot.png)

## Instalación

Para usar este tema y sus diferentes funciones se requiere un plugin extra para los campos personalizados creados en el:
 
*   "Plugins necesarios"  : **Advanced Custom Fields **

![ACF](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/ACF.png)

## Pasos

1. Clone o descargue el repositorio de **Bitbucket**
2. Copie la carpeta *wp-content* en el directorio de su proyecto de WordPress
3. Una vez hecho esto el tema y el plugin creados quedarán registrados en WordPress

## Activar Tema

- Recuerde tener instalado y activado el Plugin mencionado anteriormente **Advanced Custom Fields**
- El archivo **acf_fields.json** deberá subirlo en la parte de herramientas del Plugin **Advanced Custom Fields** en su sección de herramientas 

![Herramientas](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/herramientas.png)
![Import](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/importar.png)

- Vaya a la sección de apariencia de WordPress y encontrará el tema **"Giving Theme"** en el listado de temas y haga clic en activar
- Para activar el *Plugin "Portafolio"* vaya a la sección de plugins en WordPress en el listado encontrará el plugin y haga clic en *"activar"*
- El plugin creara una nueva  opción en el menú de administración llamada "Portafolios" 

![Import](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/Portafolios.png)

- Una vez activado el tema y el plugin ya quedarán listos para usarlos

## Edición del HOME PAGE

Luego de instalar y activar el tema y el plugin editaremos nuestra página **"HOME"**

- Ir a páginas > Añadir nueva
- Seleccionar la plantilla **"Giving Home"**

![Import](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/Plantilla.png)

- Una vez seleccionemos esta plantilla apareceran los campos personalizados ya configurados para agregar contenido a nuestro *"Home"*

## Widget Portafolio

En la sección de Apariencia > Widgets encontrara un recuadro llamado *"Post portafolio"* allí deberá arrastrar el widget y una vez guardado los cambios y agregados unos cuantos posts de *"Portafolios"* podrá visualizar las entradas en el *"Home"*

Imagen del nuevo tipo de entradas creado:

![Widget](https://bitbucket.org/jdda95/giving-theme/raw/ccc29a0d36d12a9c2e56d3c0a3ced11f59ff4148/support-images/widget.png)
