<?php

    function init_template(){
        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');

        register_nav_menus( 
            array(
                'giving-home' => 'Giving Home'
            )
        );
    }
    add_action( 'after_setup_theme', 'init_template');

    function assets(){
        wp_register_style( 'material', 'https://fonts.googleapis.com/icon?family=Material+Icons', '', '1.0', 'all' );
        wp_enqueue_style('estilos', get_stylesheet_uri(), array('material') , '1.0', 'all');
        wp_enqueue_script( 'main', get_template_directory_uri().'/assets/js/main.js', array('jquery'), '1.0', true);
        wp_localize_script( 'main', 'pf', array(
            'ajaxurl' => admin_url('admin-ajax.php')
        ));
    }

    add_action( 'wp_enqueue_scripts', 'assets' );


    function pfFiltroPortafolio(){
        $args= array(
            'post_type' => 'portafolio',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'title',
        );
        if($_POST['categoria']) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'categoria-portafolios',
                    'field' => 'slug',
                    'terms' => $_POST['categoria'],
                )
            );
        }
        $portafolios = new WP_Query($args);
        if($portafolios->have_posts()){
            $return = array();
            while ($portafolios->have_posts()) {
                $portafolios->the_post();
                $terms = get_the_term_list( $portafolios->ID, 'categoria-portafolios', '', ', ' );
                $terms = strip_tags( $terms );
                $return[] = array(
                    'imagen' => get_the_post_thumbnail_url( get_the_id(), 'full' ),
                    'link' => get_the_permalink( ),
                    'titulo' => get_the_title( ),
                    'tags' => $terms
                );
            }
            wp_send_json( $return );
        }
    }
    add_action('wp_ajax_pfFiltroPortafolio', 'pfFiltroPortafolio');
    add_action('wp_ajax_nopriv_pfFiltroPortafolio', 'pfFiltroPortafolio');

    function sidebar_page(){
        register_sidebar( array(
            'name' => 'Aside interna',
            'id' => 'intern-pf',
            'description' => 'Interna portafolio',
            'before_title' => '<p>',
            'after_title' => '</p>',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>'
        ) );
    }

    add_action('widgets_init', 'sidebar_page');

    function wpdocs_custom_excerpt_length( $length ) {
        return 10;
    }
    add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
    
    require_once('includes/custom-comments.php');
    require_once('includes/custom-fields.php');
    require_once('includes/custom-breadcrumb.php');
?>