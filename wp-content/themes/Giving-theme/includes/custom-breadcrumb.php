<?php
    function the_breadcrumb() {
        if (!is_front_page()) {
            echo '<ul class="breadcrumbs">';
            echo '<li><a href="';
            echo get_option('home');
            echo '">Home';
            echo '</a></li>';

            if (is_category() || is_single() ){
                echo '<li><a href="">';
                bloginfo('name');
                echo '</a></li>';
            }
        
            if (is_single()) {
                echo '<li><a href="">';
                the_title();
                echo '</a></li>';
            }
        
            if (is_page()) {
                echo '<li><a href="">';
                the_title();
                echo '</a></li>';
            }

            echo '</ul>';
        }
    }
?>