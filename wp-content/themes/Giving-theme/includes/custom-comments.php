<?php

/**
 * Personalización de formulario de comentarios
 * 
 * @author Julian Delgado
 * @since 1.0.0
 */


 function pf_modify_comment_field($fields) {
     $fields = array(
        'author' => '<span class="comment-form-author"><label for="author">Name</label><input id="author" name="author" aria-required="true" placeholder="' . $comment_author .'"></input></span>',
        'email' => '<span class="comment-form-email"><label for="email">Email</label><input id="email" name="email" placeholder="' . $comment_email .'"></input></span>',
     );
     return $fields;
 }

 add_filter('comment_form_default_fields', 'pf_modify_comment_field');

?>