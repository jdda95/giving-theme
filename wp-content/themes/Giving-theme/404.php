<?php get_header(); ?>

<main class="container">
    <div class="pagina404 my-5">
        <h1>404 pagina no encontrada</h1>
        <h2>Haga <a href="<?= home_url(); ?>"> click aquí </a> para volver a la página principal</h2>
    </div>
</main>

<?php get_footer(); ?>