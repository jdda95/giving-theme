<?php
/**
 * The searchform.php template.
 */
?>
<div class="search">
    <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" id="searchForm" autocomplete="off">
        <input type="search" name="search" id="search" placeholder="Search">
        <button type="submit"><i class="material-icons">search</i></button>
    </form>
</div>