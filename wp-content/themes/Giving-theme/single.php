<?php get_header(); ?>
    <div class="single_banner">
        <?php if(have_posts()){
        while(have_posts()){
        the_post(); ?>
            <h2 class="uppercase"><?php the_title(); ?></h2>
        <?php } }?>
        <?php the_breadcrumb(); ?>
    </div>
<div class="single_post">
    <div class="container">
        <aside>
            <?php dynamic_sidebar( 'intern-pf' );?>
            <?php get_template_part('template-parts/aside', 'posts'); ?>
        </aside>
            <section>
                <?php if(have_posts()){
                while(have_posts()){
                the_post(); ?>
                    <img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php the_title(); ?>">
                    <div class="desc">
                        <?php  the_content(); ?>
                    </div>
                <?php } }?>
                <?php
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                ?>
            </section>
    </div>
</div>
<footer>
    <h1 class="uppercase">ADAM</h1>
    <ul class="social">
        <li><a href="<?= $fields['link_facebook'] ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/fb.svg" alt="facebook"></a></li>
        <li><a href="<?= $fields['link_twitter'] ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/tw.svg" alt="twitter"></a></li>
        <li><a href="<?= $fields['link_linkedin'] ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/in.svg" alt="linkedin"></a></li>
        <li><a href="<?= $fields['link_instagram'] ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/ins.svg" alt="instagram"></a></li>
        <li><a href="<?= $fields['link_dribbble'] ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/dr.svg" alt="dribbble"></a></li>
    </ul>
    <small>ALL © RESERVED <a href="">WPDNA</a></small>
</footer>


<?php get_footer(); ?>