(function($){
    $('.filters li a').click(function (e) {
        $('.filters li.active').removeClass('active');
        $(e.target).parents('li').addClass('active');
        $.ajax({
            url: pf.ajaxurl,
            method: "POST",
            data: {
                "action": 'pfFiltroPortafolio',
                "categoria": $(e.target).data('term')
            },
            beforeSend: function() {
                $('#products').html('Cargando...');
            },
            success: function(data) {
                let html = '';
                data.forEach(item => {
                    html += `
                    <div class="products_item">
                        <img src="${item.imagen}" alt="imagen">
                        <div class="desc">
                            <h3 class="light">${item.titulo}</h3>
                            <small>${item.tags}</small>
                        </div>
                    </div>
                    `;
                    $('#products').html(html);
                });
            },
            error: function(error) {
                console.log(error);
            }
        })
    });
    document.querySelector('#menuBtn').addEventListener('click', function() {
        document.querySelector('nav').classList.toggle('active');
    });
    
    document.querySelector('.upPage').addEventListener('click', function() {
        window.scroll({top: 0, left: 0, behavior: 'smooth' });
    });
    
    window.onscroll = function() {scrollFunction()};
    
    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.querySelector('.upPage').style.display = "block";
      } else {
        document.querySelector('.upPage').style.display = "none";
      }
    }
})(jQuery);