<?php
    $comments_number = get_comments_number();
    if($comments_number > 0) {
        
?>
    <div class="comments">
        <h2 class="uppercase">Have <?=$comments_number?> comments</h2>
        <ul class="comments-list">
        <?php wp_list_comments( array(
            'callback' => function($comment, $args, $depth){?>
            <li>
                <div class="photo">
                    <?php
                        if ( $args['avatar_size'] != 0 ) {
                            echo get_avatar( $comment, $args['avatar_size'] ); 
                        } 
                    ?>
                </div>
                <div class="comment-content">
                    <h4 class="author">
                        <?php
                            printf( __( '<cite class="fn">%s:</cite> <span class="says">says:</span>' ), get_comment_author_link() );
                        ?>
                    </h4>
                    <small><?php
                        printf( 
                            __('%1$s'),
                            get_comment_date()
                        );
                    ?></small>
                    <?php  comment_text(); ?>
                </div>
                <?php 
                    if ( $comment->comment_approved == '0' ) { ?>
                        <em><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php 
                    }
                ?>
                <div class="reply" id="reply"><?php 
                        comment_reply_link( 
                            array_merge( 
                                $args, 
                                array( 
                                    'reply_text' => '<i class="material-icons">reply</i>', 
                                    'depth'     => $depth, 
                                    'max_depth' => $args['max_depth'] 
                                ) 
                            ) 
                        ); ?>
                </div>
            </li>
        <?php }
        ) ); ?>
        </ul>
    </div>
<?php } ?>

<div class="form-comment">
    <?php comment_form(
        array(
            'title_reply' => __('Leave your thought', 'apk'),
            'label_submit' => __('POST COMMENT', 'apk'),
            'comment_field' => '<span class="txt-area"><label for="comment">Textarea</label><textarea name="comment" id="comment" cols="30" rows="10"></textarea></span>',
            'comment_notes_before' => '',
            'comment_notes_after' => '',
            'logged_in_as' => '',
            'reverse_top_level' => true
        )

    ); ?>
</div>