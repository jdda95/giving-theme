<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head() ?>
</head>
<body>

<header>
    <div class="container">
        <h1 class="uppercase">ADAM</h1>
        <button id="menuBtn">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <nav>
            <?php wp_nav_menu(
                array(
                    'menu_class'    => 'menu-principal',
                    'container_class' => 'container-menu',
                )
            ); 
            ?>
        </nav>
    </div>
</header>