<?php 
   $the_query = new WP_Query( array(
     'category_name' => 'blog',
      'posts_per_page' => 3,
   )); 
?>
<ul class="posts">
<?php if ( $the_query->have_posts() ) : ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<li>
  <img src="<?php the_post_thumbnail_url('large'); ?>" alt="imagen">
  <div class="desc">
      <div class="top">
        <h3 class="light"><?php the_title(); ?></h3>
        <small class="uppercase light"><?php get_the_date(); ?></small>
        <?php the_excerpt(); ?>
      </div>
      <div class="actions">
          <div class="left">
              <a href=""><i class="material-icons">reply</i></a>
              <a href=""><i class="material-icons">comment</i></a>
              <a href=""><i class="material-icons">favorite_border</i></a>
          </div>
          <a href="<?php the_permalink(); ?>">Read more</a>
      </div>
  </div>
</li>
  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
</ul>
<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>