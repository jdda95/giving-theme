<?php 
   $the_query = new WP_Query( array(
     'category_name' => 'blog',
      'posts_per_page' => 3,
   )); 
?>
<div class="recentpost">
    <h2>Recent Posts</h2>
    <ul class="recentpost-list">
        <?php if ( $the_query->have_posts() ) { ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li>
            <a href="<?php the_permalink(the_ID()); ?>">
                <img src="<?php the_post_thumbnail_url('large'); ?>" alt="image">
                <div class="info">
                    <h3 class="uppercase"><?php the_title(); ?></h3>
                    <?php the_excerpt(); ?>
                </div>
            </a>
        </li>
        <?php endwhile; } ?>
        <?php wp_reset_postdata(); ?>
    </ul>
</div>