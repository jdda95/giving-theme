<?php
//Template name: Giving Home
    get_header();
    $fields = get_fields();
?>

<main>
    <div class="banner" style="background-image: url(<?= $fields['imagen_banner'] ? $fields['imagen_banner'] : get_template_directory_uri() . '/assets/img/img1.jpg' ?>);">
        <div class="banner_box">
            <h2 class="uppercase"><?= $fields['titulo_banner'] ? $fields['titulo_banner'] : 'Minimal Portfolio' ?></h2>
            <p><?= $fields['subtitulo_banner'] ? $fields['subtitulo_banner'] : 'Portfolio for creatives' ?></p>
        </div>
    </div>
    <div class="container">
        <ul class="filters">
            <li class="uppercase light active">
                <a href="javascript:;"  data-term="">All</a>
                <div class="dots">
                    <div class="dots_item"></div>
                    <div class="dots_item"></div>
                    <div class="dots_item"></div>
                </div>
            </li>
        <?php
            $terms = get_terms('categoria-portafolios', array('hide_empty' => true));
            foreach ($terms as $term) {
                echo '<li class="uppercase light"><a href="javascript:;" data-term="'.$term->slug.'">'.$term->name.'</a><div class="dots"><div class="dots_item"></div><div class="dots_item"></div><div class="dots_item"></div></div></li>';
            }
        ?>
        </ul>
        <div class="products" id="products">
            <?php dynamic_sidebar( 'item-portafolio' );?>
        </div>
    </div>
    <div class="last_posts">
        <div class="container">
            <div class="intro">
                <h2 class="bold uppercase"><?= $fields['title_latest_posts'] ?></h2>
                <p><?= $fields['desc_latest_posts'] ?></p>
            </div>
            <?php get_template_part('template-parts/recent', 'post'); ?>
        </div>
    </div>
</main>
<footer>
    <h1 class="uppercase">ADAM</h1>
    <ul class="social">
        <li><a href="<?= $fields['link_facebook'] ? $fields['link_facebook'] : 'https://www.facebook.com/' ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/fb.svg" alt="facebook"></a></li>
        <li><a href="<?= $fields['link_twitter'] ? $fields['link_twitter'] : '' ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/tw.svg" alt="twitter"></a></li>
        <li><a href="<?= $fields['link_linkedin'] ? $fields['link_linkedin'] : 'https://www.linkedin.com/' ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/in.svg" alt="linkedin"></a></li>
        <li><a href="<?= $fields['link_instagram'] ? $fields['link_instagram'] : 'https://www.instagram.com/' ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/ins.svg" alt="instagram"></a></li>
        <li><a href="<?= $fields['link_dribbble'] ? $fields['link_dribbble'] : 'https://dribbble.com/' ?>" target="_blank"><img src="<?=get_template_directory_uri();?>/assets/icons/dr.svg" alt="dribbble"></a></li>
    </ul>
    <small>ALL © RESERVED <a href="">WPDNA</a></small>
</footer>

<?php get_footer(); ?>