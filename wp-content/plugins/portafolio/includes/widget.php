<?php

class portafolio_widget extends WP_Widget {

    function portafolio_widget(){
        $widget_ops = array('classname' => 'pf_widget', 'description' => "Traer todos los posts de portafolio" );
        $this->WP_Widget('pf_widget', "Widget portafolio", $widget_ops);    }

    function widget($args,$instance){
         echo $before_widget;
        ?>
        <?php  
        $args = array(
            'post_type' => 'portafolio',
            'post_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'title'

        );
            $productos = new WP_Query($args);
            if($productos->have_posts()){
                while($productos->have_posts()) {
                    $productos->the_post();
        ?>
            <a href="<?php the_permalink(); ?>" class="products_item">
                <img src="<?php the_post_thumbnail_url( 'full' ) ?>" alt="<?php  the_title(); ?>">
                <div class="desc">
                    <h3 class="light"><?php  the_title(); ?></h3>
                    <?php 
                        $terms = get_the_term_list( $productos->ID, 'categoria-portafolios', '', ', ' );
                        $terms = strip_tags( $terms );
                        echo '<small>'.$terms.'</small>';
                    ?>
                </div>
            </a>
        <?php
                }
            }
        ?>
        <?php
        echo $after_widget;
    }
}

?>