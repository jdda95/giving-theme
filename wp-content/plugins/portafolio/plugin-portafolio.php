<?php

    //Plugin name: Portafolio
    //Description: Este plugin te permite crear un widget para crear items del portafolio 
    //Version: 1.0
    //Author: Julian Delgado
    //Author URI: https://juliandelgado.dev/

    function estilos_plugin() {
        $estilos_url = plugin_dir_url(__FILE__);
        wp_enqueue_style('portafolio', $estilos_url.'/assets/css/estilos.css' , '', '1.0', 'all');
    }
    
    add_action( 'wp_enqueue_scripts', 'estilos_plugin');

    function portafolios_type(){
        $labels = array(
            'name' => 'Portafolios',
            'singular_name' => 'Portafolio',
            'menu_name' => 'Portafolios'
        );
        $args = array(
            'label' => 'portafolios',
            'description' => 'Portafolios del sitio',
            'labels' => $labels,
            'supports' => array('title','editor', 'categoria-portafolios', 'etiqueta-portafolios', 'thumbnail', 'revisions', 'comments'),
            'public' => true,
            'taxonomies' => array('categoria-portafolios'),
            'show_in_menu' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-media-document',
            'can_export' => true,
            'publicly_queryable' => true,
            'rewrite' => true,
            'show_in_rest' => true
        );
        register_post_type( 'portafolio', $args );
    }
    add_action('init', 'portafolios_type');


    function pgRegisterTax(){
        $args = array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Categorías de portafolios',
                'singular_name' => 'Categoría de portafolios'
            ),
            'show_in_nav_menu' => true,
            'show_admin_column' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_rest' => true,
            'rewrite' => array('slug' => 'categoria-portafolios')
        );
        register_taxonomy( 'categoria-portafolios', array('portafolio'), $args );
    }
    add_action('init', 'pgRegisterTax');

    function tagPortfolio(){
        $args = array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Etiquetas de portafolios',
                'singular_name' => 'Etiquetas de portafolios'
            ),
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menu' => true,
            'show_admin_column' => true,
            'show_in_rest' => true,
            'rewrite' => array('slug' => 'etiqueta-portafolios')
        );
        register_taxonomy( 'etiqueta-portafolios', array('portafolio'), $args );
    }
    add_action('init', 'tagPortfolio');

    
    function sidebar(){
        register_sidebar( array(
            'name' => 'Posts portafolio',
            'id' => 'item-portafolio',
            'description' => 'Posts portafolio',
            'before_title' => '<p class="hidden">',
            'after_title' => '</p>',
            'before_widget' => '<div id="%1$s" class="%2$s products_item">',
            'after_widget' => '</div>'
        ) );
    }

    add_action('widgets_init', 'sidebar');
    
    /**
     * Función que instancia el Widget
    */
    function pf_create_widget(){
        include_once(plugin_dir_path( __FILE__ ).'/includes/widget.php');
        register_widget('portafolio_widget');
    }
    add_action('widgets_init','pf_create_widget');

?>